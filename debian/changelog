colorpicker (1.0.0-4) unstable; urgency=medium

  * Team upload.
  * Rename debian/README.Debian-source to debian/README.source
  * Debhelper compat 13
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Fix doc-base

 -- Andreas Tille <tille@debian.org>  Fri, 24 Jan 2025 08:36:17 +0100

colorpicker (1.0.0-3) unstable; urgency=medium

  * Team upload.
  * Switch to compat level 10.
  * wrap-and-sort -sa.
  * Use canonical Vcs address.
  * Declare compliance with Debian Policy 4.1.1.
  * Update Homepage address. There is a new fork at Github now.
  * Compile colorpicker for use with Java 8 and above.
    Thanks to Chris West for the report and patch. (Closes: #873974)

 -- Markus Koschany <apo@debian.org>  Wed, 25 Oct 2017 20:24:30 +0200

colorpicker (1.0.0-2) unstable; urgency=low

  [ Andrew Ross ]
  * Update synopsis and d/copyright to fix linitian warnings.
  * Add lintian override file since it's not possible to write
    a useful watch file.

  [ Miguel Landaeta ]
  * Team upload.
  * Bump Standards-Version to 3.9.2. No changes were required.

  [ James Page ]
  * Fix FTBFS with OpenJDK 7 (LP: #888928) (Closes: #651398):
    - d/rules: Override jh_build and specify source/target of 1.5 for
      javac and javadoc commands to ensure backwards compatibility and
      work around Java 7 encoding errors.

 -- Andrew Ross <ubuntu@rossfamily.co.uk>  Fri, 16 Dec 2011 18:49:12 +0000

colorpicker (1.0.0-1) unstable; urgency=low

  * Initial release of ColorPicker package. Source package is repackaged
    from upstream zip with no changes. (Closes: #602014)

 -- Andrew Ross <ubuntu@rossfamily.co.uk>  Sun, 31 Oct 2010 19:48:12 +0000
